﻿namespace PAS.share.DTOs.Responses;
public class DataResponse<T>
{
    public DataResponse()
    {
    }

    public DataResponse(T data)
    {
        data = Data;
    }

    public T Data { get; set; }      
}