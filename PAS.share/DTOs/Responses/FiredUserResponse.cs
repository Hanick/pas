﻿using PAS.data.Entities;

namespace PAS.share.DTOs.Responses;

public class FiredUserResponse
{
    public long Id { get; set; }
    public string? FirstName { get; set; }
    public string? LastName { get; set; }
    public string? CompanyName { get; set; }

    public FiredUserResponse()
    {
    }

    public FiredUserResponse(FiredUser firedUser)
    {
        Id = firedUser.FiredUserId;
        FirstName = firedUser.FirstName;
        LastName = firedUser.LastName;
        CompanyName = firedUser.CompanyName;
    }
}