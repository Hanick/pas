﻿namespace PAS.share.Models;

public class PagingModel
{
    public int PageNumber { get; set; } = 1;
    public int PageSize { get; set; } = 10;
}

