﻿namespace PAS.share.Models;

public class UserSortingModel : PagingModel
{
    public long? SearchById { get; set; }
    public string? SearchByFirstName { get; set; }
    public string? SearchByLastName { get; set; }
    public string SortBy { get; set; } = "id";
    public bool IsAscending { get; set; } = true;
}