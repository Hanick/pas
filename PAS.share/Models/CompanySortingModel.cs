﻿namespace PAS.share.Models;

public class CompanySortingModel : PagingModel
{
    public long? SearchById { get; set; }
    public string? SearchByCompanyName { get; set; }
    public string SortBy { get; set; } = "id";
    public bool IsAscending { get; set; } = true;
}
