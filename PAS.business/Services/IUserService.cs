﻿using PAS.share.DTOs.Responses;
using PAS.share.Models;

namespace PAS.business.Services;

public interface IUserService
{
    FiredUserResponse FireUser(long id);
}

