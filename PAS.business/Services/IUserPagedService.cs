﻿using PAS.data.Entities;
using PAS.share.DTOs.Responses;
using PAS.share.Models;

namespace PAS.business.Services;

public interface IUserPagedService
{
    PagingResponse<IQueryable<User>> GetPagedUsers(UserSortingModel userSorting);
}

