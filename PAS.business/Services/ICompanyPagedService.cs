﻿using PAS.data.Entities;
using PAS.share.DTOs.Responses;
using PAS.share.Models;

namespace PAS.business.Services;

public interface ICompanyPagedService
{
    PagingResponse<IQueryable<Company>> GetPagedCompanies(CompanySortingModel companySorting);
}