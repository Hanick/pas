﻿using Microsoft.EntityFrameworkCore;
using PAS.business.Helpers;
using PAS.data.Entities;
using PAS.data.Repositories;
using PAS.share.DTOs.Responses;
using PAS.share.Models;

namespace PAS.business.Services.Impl;

public class UserPagedService : IUserPagedService
{
    private readonly IUserRepository _userRepository;

    public UserPagedService(IUserRepository userRepository)
    {
        _userRepository = userRepository;
    }

    public PagingResponse<IQueryable<User>> GetPagedUsers(UserSortingModel userSorting)
    {
        var users = _userRepository.GetAll();

        if (userSorting.SearchById.HasValue)
            users = users.Where(x => x.UserId == userSorting.SearchById);
        if (!string.IsNullOrEmpty(userSorting.SearchByFirstName))
            users = users.Where(x => x.FirstName != null && x.FirstName.ToLower().Contains(userSorting.SearchByFirstName.ToLower().Trim()));
        if (!string.IsNullOrEmpty(userSorting.SearchByLastName))
            users = users.Where(x => x.LastName != null && x.LastName.ToLower().Contains(userSorting.SearchByLastName.ToLower().Trim()));

        users = userSorting.SortBy.ToLower() switch
        {
            "firstname" => userSorting.IsAscending ? users.OrderBy(x => x.FirstName) : users.OrderByDescending(x => x.FirstName),
            "lastname" => userSorting.IsAscending ? users.OrderBy(x => x.LastName) : users.OrderByDescending(x => x.LastName),
            "id" => userSorting.IsAscending ? users.OrderBy(x => x.UserId) : users.OrderByDescending(x => x.UserId),
            _ => users.OrderBy(x => x.UserId),
        };

        var res = users.ToPagedResponse(userSorting);
        return res;
    }
}