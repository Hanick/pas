﻿using PAS.data.Repositories;

namespace PAS.business.Services.Impl;

public class CompanyService : ICompanyService
{
    private readonly ICompanyRepository _companyRepository;

    public CompanyService(ICompanyRepository companyRepository)
    {
        _companyRepository = companyRepository;
    }

    public void RemoveCompany(long id)
    {
        _companyRepository.CompanyRemove(id);
    }
}


