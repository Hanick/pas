﻿using Microsoft.EntityFrameworkCore;
using PAS.business.Helpers;
using PAS.data.Entities;
using PAS.data.Repositories;
using PAS.share.DTOs.Responses;
using PAS.share.Models;

namespace PAS.business.Services.Impl;

public class CompanyPagedService : ICompanyPagedService
{
    private readonly ICompanyRepository _companyRepository;

    public CompanyPagedService(ICompanyRepository companyRepository)
    {
        _companyRepository = companyRepository;
    }

    public PagingResponse<IQueryable<Company>> GetPagedCompanies(CompanySortingModel companySorting)
    {
        var company = _companyRepository.GetAll();

        if (companySorting.SearchById.HasValue)
            company = company.Where(x => x.CompanyId == companySorting.SearchById);
        if (!string.IsNullOrEmpty(companySorting.SearchByCompanyName))
            company = company.Where(x => x.CompanyName != null && x.CompanyName.ToString()!.ToLower().Contains(companySorting.SearchByCompanyName.ToLower().Trim()));

        company = companySorting.SortBy.ToLower() switch
        {
            "companyname" => companySorting.IsAscending ? company.OrderBy(x => x.CompanyName) : company.OrderByDescending(x => x.CompanyName),
            "id" => companySorting.IsAscending ? company.OrderBy(x => x.CompanyId) : company.OrderByDescending(x => x.CompanyId),
            _ => company.OrderBy(x => x.CompanyId),
        };

        var res = company.Include(x => x.Users).ToPagedResponse(companySorting);

        return res;
    }
}