﻿using PAS.data.Entities;
using PAS.data.Repositories;
using PAS.share.DTOs.Responses;

namespace PAS.business.Services.Impl;

public class UserService : IUserService
{
    private readonly IUserRepository _userRepository;
    private readonly IFiredUserRepository _firedUserRepository;

    public UserService(IUserRepository userRepository, IFiredUserRepository firedUserRepository)
    {
        _userRepository = userRepository;
        _firedUserRepository = firedUserRepository;
    }

    public FiredUserResponse FireUser(long id)
    {
        var res = _userRepository.FindById(id);
        _userRepository.Remove(res);

        var newUser = new FiredUser
        {
            FirstName = res.FirstName,
            LastName = res.LastName,
            CompanyName = res.Company?.CompanyName?.ToString(),
        };
        _firedUserRepository.AddUser(newUser);

        return new FiredUserResponse(newUser);
    }
}