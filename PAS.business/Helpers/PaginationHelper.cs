using PAS.share.DTOs.Responses;
using PAS.share.Models;

namespace PAS.business.Helpers;

public static class PaginationHelper
{
    public static PagingResponse<IQueryable<T>> ToPagedResponse<T>(this IQueryable<T> pagedData, PagingModel validFilter)
    {
        var response = new PagingResponse<IQueryable<T>>(pagedData.Skip((validFilter.PageNumber - 1) * validFilter.PageSize)
            .Take(validFilter.PageSize), validFilter.PageNumber, validFilter.PageSize);
        var totalPages = pagedData.Count() / (double)validFilter.PageSize;
        var roundedTotalPages = Convert.ToInt32(Math.Ceiling(totalPages));

        response.TotalPages = roundedTotalPages;
        response.TotalRecords = pagedData.Count();

        return response;
    }
}   