using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using PAS.business.Services;
using PAS.business.Services.Impl;
using PAS.data;
using PAS.data.Repositories;
using PAS.data.Repositories.Impl;
var builder = WebApplication.CreateBuilder(args);

builder.Services.AddScoped(typeof(IRepositoryBase<>), typeof(RepositoryBase <>));
builder.Services.AddScoped<IUserRepository, UserRepository>();
builder.Services.AddScoped<IFiredUserRepository, FiredUserRepository>();
builder.Services.AddScoped<ICompanyRepository, CompanyRepository>();


builder.Services.AddScoped<IUserPagedService, UserPagedService>();
builder.Services.AddScoped<ICompanyPagedService, CompanyPagedService>();
builder.Services.AddScoped<IUserService, UserService>();
builder.Services.AddScoped<ICompanyService, CompanyService>();

builder.Services.AddAutoMapper(typeof(Program));

builder.Services.AddControllers();

builder.Services.AddEndpointsApiExplorer();

builder.Services.AddSwaggerGen();

builder.Services.AddDbContext<DataContext>(opt =>
                opt.UseNpgsql(builder.Configuration.GetConnectionString("DefaultConnection")));

var app = builder.Build();

if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
