﻿using Microsoft.AspNetCore.Mvc;
using PAS.business.Services;
using PAS.share.Models;

namespace PAS.api.Controllers;

[ApiController]
[Route("api/Companies")]
public class CompanyController : ControllerBase
{
    private readonly ICompanyPagedService _companyPagedService;
    private readonly ICompanyService _companyService;

    public CompanyController(ICompanyPagedService companyPagedService, ICompanyService companyService)
    {
        _companyPagedService = companyPagedService;
        _companyService = companyService;
    }

    [HttpGet("")]
    public IActionResult GetSort([FromQuery] CompanySortingModel companySorting)
    {
        var response = _companyPagedService.GetPagedCompanies(companySorting);

        return Ok(response);
    }

    [HttpDelete("{id:long}")]
    public IActionResult RemoveCompany(long id)
    {
        _companyService.RemoveCompany(id);
        return NoContent();
    }
}