﻿using Microsoft.AspNetCore.Mvc;
using PAS.business.Services;
using PAS.share.Models;

namespace PAS.api.Controllers;

[ApiController]
[Route("api/Users")]
public class UserController : ControllerBase
{
    private readonly IUserPagedService _userPagedService;
    private readonly IUserService _userService;

    public UserController(IUserPagedService userPagedService, IUserService userService)
    {
        _userPagedService = userPagedService;
        _userService = userService;
    }

    [HttpGet("")]
    public IActionResult GetSort([FromQuery] UserSortingModel userSorting)
    {
        var response = _userPagedService.GetPagedUsers(userSorting);

        return Ok(response);
    }

    [HttpPost("")]
    public IActionResult GetFireUser([FromQuery] FiredUserModel fireduserModel)
    {
        var response = _userService.FireUser(fireduserModel.Id);

        return Ok(response);
    }
}