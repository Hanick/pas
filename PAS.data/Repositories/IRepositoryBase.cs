﻿namespace PAS.data.Repositories;

public interface IRepositoryBase<TEntity> where TEntity : class
{
    void Create(TEntity item);
    void Remove(TEntity item);
    void Update(TEntity item);
    void SaveChanges(TEntity item);
    IQueryable<TEntity> GetAll();
    TEntity FindById(long id);
}