﻿using PAS.data.Entities;

namespace PAS.data.Repositories;

public interface IUserRepository : IRepositoryBase<User>
{
    void FindAndRemove(User user);
}