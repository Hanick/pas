﻿using Microsoft.EntityFrameworkCore;
using PAS.data.Entities;

namespace PAS.data.Repositories.Impl;

public class CompanyRepository : RepositoryBase<Company>, ICompanyRepository
{
    public CompanyRepository(DataContext context) : base(context)
    {
    }

    public void CompanyRemove(long id)
    {
        var company = GetAll().Include(x => x.Users).First(x => x.CompanyId == id);
        company.Users?.Clear();
        Remove(company);
    }
}