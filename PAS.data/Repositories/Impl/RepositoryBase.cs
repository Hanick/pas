﻿using Microsoft.EntityFrameworkCore;

namespace PAS.data.Repositories.Impl;

public class RepositoryBase<TEntity> : IRepositoryBase<TEntity> where TEntity : class
{
    protected readonly DataContext _context;
    protected readonly DbSet<TEntity> _dbSet;

    public RepositoryBase(DataContext context)
    {
        _context = context;
        _dbSet = context.Set<TEntity>();
    }

    public void Create(TEntity item)
    {
        _dbSet.Add(item);
        _context.SaveChanges();
    }

    public TEntity FindById(long id)
    {
        return _dbSet.Find(id);
    }

    public IQueryable<TEntity> GetAll()
    {
        return _dbSet;
    }

    public void Remove(TEntity item)
    {
        _dbSet.Remove(item);
        _context.SaveChanges();
    }

    public void SaveChanges(TEntity item)
    {
        _context.SaveChanges();
    }

    public void Update(TEntity item)
    {
        _context.Entry(item).State = EntityState.Modified;
        _context.SaveChanges();
    }
}