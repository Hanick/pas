﻿using PAS.data.Entities;

namespace PAS.data.Repositories.Impl;

public class FiredUserRepository : RepositoryBase<FiredUser>, IFiredUserRepository
{
    public FiredUserRepository(DataContext context) : base(context)
    {
    }

    public long AddUser(FiredUser item)
    {
        var res = _dbSet.Add( new FiredUser
        {
            FiredUserId = item.FiredUserId,
            FirstName = item.FirstName,
            LastName = item.LastName,
            CompanyName = item.CompanyName,
        });
        _context.SaveChanges();

        return res.Entity.FiredUserId;
    }
}