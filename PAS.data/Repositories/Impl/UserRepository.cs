﻿using PAS.data.Entities;

namespace PAS.data.Repositories.Impl;

public class UserRepository : RepositoryBase<User>, IUserRepository
{

    public UserRepository(DataContext context) : base(context)
    {
    }

    public void FindAndRemove(User user)
    {
        FindById(user.UserId);
        Remove(user);
    }
}