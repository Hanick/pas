﻿using PAS.data.Entities;

namespace PAS.data.Repositories;

public interface ICompanyRepository : IRepositoryBase<Company>
{
    void CompanyRemove(long id);
}

