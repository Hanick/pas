﻿using PAS.data.Entities;

namespace PAS.data.Repositories;

public interface IFiredUserRepository : IRepositoryBase<FiredUser>
{
    long AddUser(FiredUser item);
}

