﻿using Microsoft.EntityFrameworkCore;
using PAS.data.Entities;

namespace PAS.data;

public class DataContext : DbContext
{
    public DataContext()
    {
    }

    public DbSet<User> Users { get; set; }
    public DbSet<Company> Companies { get; set; }
    public DbSet<FiredUser> FiredUsers { get; set; }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<User>()
            .HasOne(c => c.Company)
            .WithMany()
            .HasForeignKey(p => p.CompanyInfoKey); ;

        modelBuilder.Entity<Company>()
            .HasMany(b => b.Users)
            .WithOne();
    }

    public DataContext(DbContextOptions<DataContext> options) : base(options)
    {
    } 
}