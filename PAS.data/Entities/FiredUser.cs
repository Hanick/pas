﻿using System.ComponentModel.DataAnnotations;

namespace PAS.data.Entities;

public class FiredUser
{   
    [Key]
    public long FiredUserId { get; set; }
    public string? FirstName { get; set; }
    public string? LastName { get; set; }
    public string? CompanyName { get; set; }
}

