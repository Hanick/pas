﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace PAS.data.Entities;

public class User
{  
    [Key]
    public long UserId { get; set; }
    public string? FirstName { get; set; }
    public string? LastName { get; set; }
    public long CompanyInfoKey { get; set; }
    [JsonIgnore]
    [ForeignKey("CompanyInfoKey")]
    public Company? Company { get; set; }
}