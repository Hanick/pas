﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PAS.data.Entities;

public class Company
{   
    [Key]
    public long CompanyId { get; set; }
    public List<User>? Users { get; set; }
    public string? CompanyName { get; set; }
    public string? Location { get; set; }
    public string? PhoneNumber { get; set; }
}